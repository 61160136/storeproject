/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class TestSelectProduct {
    public static void main(String []args){
        Connection conn = null;
         Database db = Database.getInstance();
        conn = db.getConnection();
        
        
        try{
            String sql = "SELECT id,name,price FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                System.out.println(id+" "+name+" "+price);
            }
        }catch(SQLException ex){
             Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);  
        }
        
      db.close();
            
    }
    }

