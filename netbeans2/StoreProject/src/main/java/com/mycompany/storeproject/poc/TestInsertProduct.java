/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class TestInsertProduct {
     public static void main(String []args){
        Connection conn = null;
         Database db = Database.getInstance();
        conn = db.getConnection();
        
        try{
            String sql = "INSERT INTO product (name,price)VALUES (?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"KoKo");
            stmt.setDouble(2, 20);
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row"+row+" id:"+id);
        }catch(SQLException ex){
             Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);  
        }
        
       db.close();
            
    }
}
